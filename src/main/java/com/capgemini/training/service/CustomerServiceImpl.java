package com.capgemini.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.training.bean.Customer;
import com.capgemini.training.dao.CustomerDAO;
import com.capgemini.training.exception.CustomerException;

@Service
public class CustomerServiceImpl implements CustomerService{
	@Autowired
	private CustomerDAO customerDao;
	
	@Override
	public Customer getCustomerById(Integer id) throws CustomerException {
		try {
			return customerDao.getCustomerById(id);
		}catch(Exception e) {
			throw new CustomerException(e.getMessage(),e);
		}
	}

	@Override
	public List<Customer> getAllCustomers() throws CustomerException {
		try {
			return customerDao.getAllCustomers();
		}catch(Exception e) {
			throw new CustomerException(e.getMessage(),e);
		}
	}

}
