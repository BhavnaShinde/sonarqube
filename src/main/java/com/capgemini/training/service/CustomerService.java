package com.capgemini.training.service;

import java.util.List;

import com.capgemini.training.bean.Customer;
import com.capgemini.training.exception.CustomerException;

public interface CustomerService {
	public Customer getCustomerById(Integer id) throws CustomerException;
	public List<Customer> getAllCustomers() throws CustomerException;
}
