package com.capgemini.training.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Configuration
@Slf4j
public class CustomerAspect {
	
//	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Pointcut("execution(* com.capgemini.training.controller.CustomerController.*(..))")
	public void getCustomerPointcut() {
		
	}
	
//	@Before("execution(* com.capgemini.training.controller.CustomerController.*(..))")
	@Before("getCustomerPointcut()")
	public void beforeAdvice(JoinPoint joinPoint) {	
		log.info("Before execution of "
				+joinPoint.getClass().getName()+"."
				+joinPoint.getSignature().getName()+ " method");
		System.out.println("Before execution of "
					+joinPoint.getClass().getName()+"."
							+joinPoint.getSignature().getName()+ " method");		
		
	}
	
	@After("execution(* com.capgemini.training.controller.CustomerController.*(..))")
	public void afterAdvice(JoinPoint joinPoint) {		
		log.info("After execution of "
					+joinPoint.getClass().getName()+"."
							+joinPoint.getSignature().getName()+ " method");
		System.out.println("After execution of "
					+joinPoint.getClass().getName()+"."
							+joinPoint.getSignature().getName()+ " method");		
		
	}
	
	@Before("execution(* com.capgemini.training.bean.Customer.get*(..))")
	public void beforeGetAdvice(JoinPoint joinPoint) {
		log.info("Before execution of getter methods");
		System.out.println("Before execution of getter methods");		
		System.out.println("ClassName: "+joinPoint.getClass().getName());
		System.out.println("Method Name: "+joinPoint.getSignature().getName());
	}
	
	
}
