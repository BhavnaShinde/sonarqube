package com.capgemini.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.capgemini.training.bean.Customer;
import com.capgemini.training.exception.CustomerException;
import com.capgemini.training.service.CustomerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customers/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable Integer id){
		try {
			Customer customer= customerService.getCustomerById(id);
			System.out.println(customer.getCustomerName());
			log.info("Customer: "+ customer.getCustomerId()+" details");
			return new ResponseEntity<>(customer,HttpStatus.OK);
		}catch(CustomerException e) {
			log.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
		}
	}
	
	
	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getAllCustomers(){
		try {
			List<Customer> customerList= customerService.getAllCustomers();
			log.info("Displaying All Customers");
			return new ResponseEntity<>(customerList,HttpStatus.OK);
		}catch(CustomerException e) {
			log.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
		}
	}
}
