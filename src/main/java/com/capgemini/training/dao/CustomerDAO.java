package com.capgemini.training.dao;

import java.util.List;

import com.capgemini.training.bean.Customer;

public interface CustomerDAO {
	public Customer getCustomerById(Integer id) throws Exception;
	public List<Customer> getAllCustomers() throws Exception;
}
