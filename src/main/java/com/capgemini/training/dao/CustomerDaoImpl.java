package com.capgemini.training.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.capgemini.training.bean.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDAO {
	
	private static Map<Integer,Customer> customers= 
							new HashMap<>();
	static {
		Customer c1=new Customer(1010,"Smith",9878678909L);
		Customer c2=new Customer(1001,"Clarke",9878678905L);
		Customer c3=new Customer(1020,"Jones",8878678900L);
		Customer c4=new Customer(1012,"Ravi",7878670011L);
		customers.put(c1.getCustomerId(), c1);
		customers.put(c2.getCustomerId(), c2);
		customers.put(c3.getCustomerId(), c3);
		customers.put(c4.getCustomerId(), c4);
		
	}

	@Override
	public Customer getCustomerById(Integer id) throws Exception {
		try {
			Customer customer= customers.get(id);
			if(customer==null) {
				throw new NullPointerException("Invalid Id");
			}
			return customer;
		}catch(Exception e) {
			throw e;
		}
		
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		try {
			
			if(customers.size()==0) {
				throw new NullPointerException("No Customers in database");
			}
			List<Customer> customerList=new ArrayList<>();
			for(Map.Entry<Integer,Customer> c: customers.entrySet()) {
				customerList.add( c.getValue());
			}			
			return customerList;
		}catch(Exception e) {
			throw e;
		}
	}

}
