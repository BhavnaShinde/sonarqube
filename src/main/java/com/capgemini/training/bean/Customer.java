package com.capgemini.training.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Customer {
	private Integer customerId;
	private String customerName;
	private Long mobile;
}
